#include "utmatrix.h"

#include <gtest.h>

TEST(TVector, can_create_vector_with_positive_length)
{
  ASSERT_NO_THROW(TVector<int> v(5));
}

TEST(TVector, cant_create_too_large_vector)
{
  ASSERT_ANY_THROW(TVector<int> v(MAX_VECTOR_SIZE + 1));
}

TEST(TVector, throws_when_create_vector_with_negative_length)
{
  ASSERT_ANY_THROW(TVector<int> v(-5));
}

TEST(TVector, throws_when_create_vector_with_negative_startindex)
{
  ASSERT_ANY_THROW(TVector<int> v(5, -2));
}

TEST(TVector, can_create_copied_vector)
{
  TVector<int> v(10);

  ASSERT_NO_THROW(TVector<int> v1(v));
}

TEST(TVector, copied_vector_is_equal_to_source_one)
{

	const int size = 10;
	TVector<int> A(size);
	TVector<int> B = A;
	EXPECT_EQ(B, A);
}

TEST(TVector, copied_vector_has_its_own_memory)
{
	const int size = 10;
	TVector<int> A(size);
	TVector<int> B = A;
	A[0] = 1;
	EXPECT_NE(B, A);
}

TEST(TVector, can_get_size)
{
  TVector<int> v(4);

  EXPECT_EQ(4, v.GetSize());
}

TEST(TVector, can_get_start_index)
{
  TVector<int> v(2, 2);

  EXPECT_EQ(2, v.GetStartIndex());
}

TEST(TVector, can_set_and_get_element)
{
  TVector<int> v(1);
  v[0] = 1;

  EXPECT_EQ(1, v[0]);
}

TEST(TVector, throws_when_set_element_with_negative_index)
{
	TVector<int> v(2);
	ASSERT_ANY_THROW(v[-1]);
}

TEST(TVector, throws_when_set_element_with_too_large_index)
{
	TVector<int> v(2);
	ASSERT_ANY_THROW(v[MAX_VECTOR_SIZE + 1]);
}

TEST(TVector, can_assign_vector_to_itself)
{
	const int size = 2;
	TVector<int> v(size);
	for (int i = 0; i < size; i++)
		v[i] = 2;
	v = v;
	EXPECT_EQ(2, v[0]);
	EXPECT_EQ(2, v[1]);
}

TEST(TVector, can_assign_vectors_of_equal_size)
{
	const int size = 2;
	TVector<int> v1(size), v2(size);
	for (int i = 0; i < size; i++)
		v1[i] = 2;
	v2 = v1;
	EXPECT_EQ(2, v2[0]);
	EXPECT_EQ(2, v2[1]);
}

TEST(TVector, assign_operator_change_vector_size)
{
	const int size1 = 2;
	const int size2 = 5;
	TVector<int> v1(size1), v2(size2);
	v1 = v2;
	EXPECT_EQ(size2, v2.GetSize());
}

TEST(TVector, can_assign_vectors_of_different_size)
{
	const int size1 = 2;
	const int size2 = 5;
	TVector<int> v1(size1), v2(size2);
	v1[0] = 0;
	v1[1] = 1;
	v2 = v1;
	EXPECT_EQ(0, v2[0]);
	EXPECT_EQ(1, v2[1]);
}

TEST(TVector, compare_equal_vectors_return_true)
{
	const int size = 2;
	TVector<int> v1(size), v2(size);
	EXPECT_EQ(v1, v2);
}

TEST(TVector, compare_vector_with_itself_return_true)
{
	const int size = 2;
	TVector<int> v1(size);
	EXPECT_EQ(v1, v1);
}

TEST(TVector, vectors_with_different_size_are_not_equal)
{
	const int size1 = 2;
	const int size2 = 5;
	TVector<int> v1(size1), v2(size2);
	EXPECT_NE(v1, v2);
}

TEST(TVector, can_add_scalar_to_vector)
{
	const int size = 1;
	TVector<int> v(size), result;
	v[0] = 2;
	result = v + size;
	EXPECT_EQ(3, result[0]);
}

TEST(TVector, can_subtract_scalar_from_vector)
{
	const int size = 1;
	TVector<int> v(size), result;
	v[0] = 2;
	result = v - size;
	EXPECT_EQ(1, result[0]);
}

TEST(TVector, can_multiply_scalar_by_vector)
{
	const int n = 2;
	const int size = 5;
	TVector<int> v(size), result;
	v[1] = 1;
	v[4] = 2;
	result = v*n;
	EXPECT_EQ(2, result[1]);
	EXPECT_EQ(4, result[4]);
}

TEST(TVector, can_add_vectors_with_equal_size)
{
	const int size = 2;
	TVector<int> v1(size),v2(size), result;
	v1[0] = 1;
	v1[1] = 2;
	v2[0] = 1;
	v2[1] = 2;
	result = v1+v2;
	EXPECT_EQ(result[0], 2);
	EXPECT_EQ(result[1], 4);
}

TEST(TVector, cant_add_vectors_with_not_equal_size)
{
	const int size = 2;
	TVector<int> v1(size), v2(size+1);
	ASSERT_ANY_THROW(v1 + v2);
}

TEST(TVector, can_subtract_vectors_with_equal_size)
{
	const int size = 2;
	TVector<int> v1(size), v2(size), result;
	v1[0] = 1;
	v1[1] = 2;
	v2[0] = 1;
	v2[1] = 2;
	result = v1 - v2;
	EXPECT_EQ(result[0], 0);
	EXPECT_EQ(result[1], 0);
}

TEST(TVector, cant_subtract_vectors_with_not_equal_size)
{
	const int size = 2;
	TVector<int> v1(size), v2(size + 1);
	ASSERT_ANY_THROW(v1 - v2);
}

TEST(TVector, can_multiply_vectors_with_equal_size)
{
	const int size = 2;
	TVector<int> v1(size), v2(size);
	int result;
	v1[0] = 0;
	v1[1] = 1;
	v2[0] = 0;
	v2[1] = 1;
	result = v1*v2;
	EXPECT_EQ(result, 1);
}

TEST(TVector, cant_multiply_vectors_with_not_equal_size)
{
	const int size = 2;
	TVector<int> v1(size), v2(size + 1);
	ASSERT_ANY_THROW(v1 * v2);
}

